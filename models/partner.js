const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// Partner Service Schema
const PartnerServicesSchema = mongoose.Schema({
	SERVICE_ID: {
		type: String
	},
	SERVICE_NAME: {
		type: String
	},
	COMMON_PRICE: {
		type: String
	}
});

// Partner Schema
const PartnerSchema = mongoose.Schema({
	PARTNER_ID: {
		type: String
	},
	PARTNER_NAME: {
		type: String
	},
	COMPANY_INFO: {
		type: String
	},
    ON_PROMO: {
		type: String
	},
    CATEGORY_ID: {
		type: String
	},
    CITY: {
		type: String
	},
    OPERATING_HOURS_START: {
		type: String
	},
    OPERATING_HOURS_END: {
		type: String
	},
    RATING_LIFETIME: {
		type: String
	},
	PARTNER_SERVICES: [PartnerServicesSchema]
});


const Partners = module.exports = mongoose.model('Partners',PartnerSchema);

module.exports.getPartnersByServiceId = function(serviceid,callback) {
	const query = {"PARTNER_SERVICES.SERVICE_ID":serviceid}
	Partners.find(query,callback);
	//Partners.find({"ALL_PARTNERS.PARTNER_NAME":"The Sierra Spa"},callback);
}